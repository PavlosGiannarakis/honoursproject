package pavlosgiannarakis40110403.primenumbergenerationandroid;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    //Initialize UI components
    private Button start;
    private long startTime, endTime, totalTime;
    //Set test parameters
    int count = 0;
    int iterations = 10;
    int limit = 1000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Map UI components to code
        start = (Button) findViewById(R.id.Start_prime);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Run method to find primes
                FindPrimes();
                // Confirm process complete
                start.setText("Timings where saved to file.");
            }
        });
    }

    public void FindPrimes() {
        // Set file name
        String filename = "primesAndroidTimingsL" + limit + "I"+ iterations + ".txt";

        // Array of boolean values of size 'limit + 1' to store prime and non prime positions
        boolean[] isPrime = new boolean[limit + 1];

        // Loop to save timings to file
        for (int x = 0; x <= iterations; x++) {
            count = 0;
            startTime = System.nanoTime();
            // Sieve of Eratosthenes method of finding primes.
            for (int i = 2; i <= limit; i++) {
                isPrime[i] = true;
                for (int j = 2; j <= limit; j++) {
                    if (i != j && i % j == 0) {
                        isPrime[i] = false;
                        break;
                    }
                }
                if (isPrime[i]) {
                    count++;
                }
            }
            /*
            * You can get the primes found by adding a for loop
            * with a if condition so that is isPrime[i] is true
            * i is a prime.
            * */
            endTime = System.nanoTime();
            totalTime = endTime - startTime;
            // Write timings to file
            if (x == 0)
            {
                initializeTimingsFile(filename);
            }
            else if (x == iterations)
            {
                captureTimings(filename,totalTime,x);
                endfile(filename,count);
            }
            else
            {
                captureTimings(filename,totalTime,x);
            }
        }
    }

    // Initialize file by adding info about the test to the top of a new file.
    public void initializeTimingsFile(String Filename)
    {
        try{
            File file = new File (Environment.getExternalStorageDirectory(), Filename);
            FileOutputStream out = new FileOutputStream(file, false); // false is used so it overwrites previous copies
            out.write(("This file contains saved timings for androids prime number generation. \n\n").getBytes());
            out.write(("Iterations:\t" + iterations + "\n").getBytes());
            out.write(("Limit:\t" + limit + "\n").getBytes());
            out.write(("\n" + "Iteration\t\t" + "Timing\n").getBytes());
            out.flush();
            out.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // Method used to append file timings and iterations to the end of the file.
    public void captureTimings(String Filename, long timing, int iteration)
    {
        try{
            File file = new File (Environment.getExternalStorageDirectory(), Filename);
            FileOutputStream out = new FileOutputStream(file, true); // True is to append to the end of previous copies
            out.write((iteration + "\t\t\t" + timing + "\n").getBytes());
            out.flush();
            out.close();
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // Method used to end file with results
    public void endfile(String Filename, int count)
    {
        try{
            File file = new File (Environment.getExternalStorageDirectory(), Filename);
            FileOutputStream out = new FileOutputStream(file, true); // True is to append to the end of previous copies
            out.write(("\n Each timing found " +count +" primes \n").getBytes());
            out.flush();
            out.close();
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
