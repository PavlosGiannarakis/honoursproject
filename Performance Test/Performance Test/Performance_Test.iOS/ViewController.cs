﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UIKit;

namespace Performance_Test.iOS
{
    public partial class ViewController : UIViewController
    {

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Reed.Hidden = true;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
        //Set test parameters
        int lines = 1000000;
        int iterations = 100;

        partial void Write_TouchUpInside(UIButton sender)
        {
            //Set name for file of testing outputs
            string filename = string.Format("XamariniOSTimingsDBWrite{0}R{1}.txt", lines, iterations);
            var pathDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var fullPath = Path.Combine(pathDir.ToString(), filename);

            //Set database access
            string DB_ANAGRAFICA_FILENAME = "performanceData.sqlite";
            string folderPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string dbpath = Path.Combine(folderPath, DB_ANAGRAFICA_FILENAME);
            var db = new SQLiteConnection(dbpath);

            //Loop to gather sample timings
            for (int i = 0; i <= iterations; i++)
            {
                //Create stopwatch instance and start it
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                //Drop table and create a new one. 
                db.DropTable<Performance_Test>();
                db.CreateTable<Performance_Test>();

                //Fill a list and the insert it to db, faster than writting single instances
                List<Performance_Test> x = new List<Performance_Test>();
                for (int j = 0; j < lines; j++)
                {
                    x.Add(new Performance_Test());
                }
                db.InsertAll(x);

                //Stop the stopwatch
                stopwatch.Stop();

                //Calculate elapsed time
                double elapsed = stopwatch.Elapsed.Milliseconds;

                //Set file with test parameters and then add timings, iteration for each loop
                if (i != 0)
                {
                    captureTiming(fullPath, elapsed, i);
                }
                else
                {
                    initializeFile(fullPath, iterations, lines, "writing to");
                }
            }

            //Confirm Process complete
            var newTitle = "Wrote to database";
            Write.SetTitle(newTitle, UIControlState.Normal);
            Reed.Hidden = false;
        }

        partial void Reed_TouchUpInside(UIButton sender)
        {
            //Set name for file of testing outputs
            string filename = string.Format("XamariniOSTimingsDBRead{0}R{1}.txt", lines, iterations);
            var pathDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var fullPath = Path.Combine(pathDir.ToString(), filename);

            //Set database access
            string DB_ANAGRAFICA_FILENAME = "performanceData.sqlite";
            string folderPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string dbpath = Path.Combine(folderPath, DB_ANAGRAFICA_FILENAME);
            var db = new SQLiteConnection(dbpath);

            //Loop to gather sample timings
            for (int i = 0; i <= iterations; i++)
            {
                //Create stopwatch instance and start it
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                //Get table instance and read items to a list of strings
                var myItems = db.Table<Performance_Test>();
                var x = new List<String>();

                foreach (var item in myItems)
                {
                    x.Add((item.ID).ToString());
                }

                // Set table source and show data
                if (i == iterations)
                {
                    table.Source = new TableSource(x.ToArray());
                    table.ReloadData();
                }

                // Stop the stopwatch and calculate the elapsed time
                stopWatch.Stop();
                double elapsed = stopWatch.Elapsed.Milliseconds;

                //Set file with test parameters and then add timings, iteration for each loop
                if (i != 0)
                {
                    captureTiming(fullPath, elapsed, i);
                }
                else
                {
                    initializeFile(fullPath, iterations, lines, "reading from");
                }
            }

            //Confirm Process complete
            var newtitle = "Process complete.";
            Reed.SetTitle(newtitle, UIControlState.Normal);
        }

        // Add timing and iteration to file
        void captureTiming(string fullPath, double elapsed, int iteration)
        {
            StreamWriter file = new StreamWriter(fullPath, true);
            file.WriteLine(iteration + ":\t\t\t" + elapsed);
            file.Close();
        }

        //Initialize file by adding details about test and its parameters
        void initializeFile(string fullPath, int iterations, int size, string part)
        {
            StreamWriter file = new StreamWriter(fullPath, false);
            file.WriteLine("This file contains the recorded timings of Performance test part 1 for Xamarin.iOS when {0} a database.", part);
            file.WriteLine("");
            file.WriteLine("Iterations performed: {0}", iterations);
            file.WriteLine("Lines written to database: {0}", size);
            file.WriteLine("");
            file.WriteLine("Iteration \t Milliseconds ");
            file.Close();
        }
    }
}

