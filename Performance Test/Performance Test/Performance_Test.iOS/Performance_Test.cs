using SQLite;

namespace Performance_Test.iOS
{
    class Performance_Test
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string data { get; set; }
    }
}