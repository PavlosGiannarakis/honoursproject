﻿using SQLite; // Sqlite db
using System.Diagnostics; // Timings
using Android.App;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Util; // Logs
using System.IO; // Path
using System.Collections.Generic;
using Android.Media;

public class performance_test // Used as class to insert into the database
{
    [PrimaryKey, AutoIncrement, Column("_id")]
    public int Id { get; set; }
    [Column("_data")]
    public string data { get; set; }
}

namespace Performance_Test.Droid
{
    [Activity(Label = "Performance_Test.Droid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int iterations = 100; // Set sample rate
        int size = 100000; // Set ammoount of lines to write to database

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            // Next 3 lines Set DB parameters
            string DB_ANAGRAFICA_FILENAME = "performanceData.sqlite3";
            string folderPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string dbpath = Path.Combine(folderPath, DB_ANAGRAFICA_FILENAME);
            
            // Set the view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            
            // Initialize parts of the layout so that they can be used.
            Button button = FindViewById<Button>(Resource.Id.myButton1);
            Button button2 = FindViewById<Button>(Resource.Id.myButton2);
            ListView listview = FindViewById<ListView>(Resource.Id.listView1);

            button2.Visibility = ViewStates.Invisible; // Hide the second button until the data is available

            // Database initiation
            var db = new SQLiteConnection(dbpath);

            button.Click += delegate // Button to write to database
            {
                //Next 3 lines set output file path to variable
                string filename = string.Format("XamarinAndroidTimingsDBWrite{0}X{1}.txt", size, iterations);
                var pathDir = global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
                var fullPath = Path.Combine(pathDir.ToString(), "Timings", filename);

                for (int i = 0; i <= iterations; i++) // Loop until specified limit
                {
                    Stopwatch stopWatch = new Stopwatch(); // Create stopwatch
                    stopWatch.Start(); // Start the stoopwatch
                    var start = Java.Lang.JavaSystem.NanoTime(); // Capture starting Java nano time

                    db.DropTable<performance_test>(); // Drop the table of object if it exists.
                    db.CreateTable<performance_test>(); // Create a new table.
                    List<performance_test> x = new List<performance_test>(); // Create a list of objects.

                    for (int j = 0; j < size; j++) // Insert 100 objects to list
                    {
                        x.Add(new performance_test()); 
                    }
                    db.InsertAll(x); // Transfer list to database

                    var end = Java.Lang.JavaSystem.NanoTime();// Capture ending Java nano time
                    stopWatch.Stop(); // Stop the stopwatch

                    double elapsed = stopWatch.Elapsed.Milliseconds; // Convert the spotwach elapsed time to Milliseconds
                    var nanoelapsed = end - start; // Calculate java nanotime elapsed

                    if (i != 0) // Discard first entry and instead initiate file
                    {
                        captureTiming(fullPath, elapsed, nanoelapsed, i);
                    }
                    else // After the first entry add a line with the iteration and timings.
                    {
                        initializeFile(fullPath, iterations, size, "writing to");
                    }
                }
                button.Text = "Wrote to DB and saved timings"; // Confirm the action has completed
                makeVisible(fullPath); // make file visible in the system
                button2.Visibility = ViewStates.Visible; // Make the second button visible
            };

            button2.Click += delegate // Button to read from database
            {
                // Next 3 lines get data
                string filename = string.Format("XamarinAndroidTimingsDBRead{0}.txt", size);
                var pathDir = global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
                var fullPath = Path.Combine(pathDir.ToString(), "Timings", filename);//"XamarinAndroidTimingsDBRead100R.txt");

                for (int i = 0; i <= iterations; i++) // Loop until specified limit
                {
                    Stopwatch stopWatch = new Stopwatch(); // Create stopwatch
                    stopWatch.Start(); // Start the stoopwatch
                    var start = Java.Lang.JavaSystem.NanoTime(); // Capture starting Java nano time

                    List<string> myItems = new List<string>(); // Create list of strings
                    var table = db.Table<performance_test>(); // Get database table contents

                    foreach (var row in table) // Loop through contents and add them to list as a string
                    {
                        string y = row.data + " " + row.Id;
                        myItems.Add(y);
                    }

                    if (i == iterations)
                    {
                        ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, myItems); // Create adapter that uses the list
                        listview.Adapter = adapter;// Assign adapter to listview
                    }

                    var end = Java.Lang.JavaSystem.NanoTime();// Capture ending Java nano time
                    double elapsed = stopWatch.Elapsed.Milliseconds; // Convert the spotwach elapsed time to Milliseconds
                    var nanoelapsed = end - start; // Calculate java nanotime elapsed

                    if (i != 0) // Discard first entry and instead initiate file
                    {
                        captureTiming(fullPath, elapsed, nanoelapsed, i);
                    }
                    else // After the first entry add a line with the iteration and timings.
                    {
                        initializeFile(fullPath, iterations, size, "reading from");
                    }
                }
                button2.Text = "Read from DB and saved timings";// Confirm the action has completed
                makeVisible(fullPath); // make file visible in the system
            };
        }

        protected override void OnPause()
        {
            base.OnPause();

            Log.Info("myapp", "Paused");
        }

        protected override void OnResume()
        {
            base.OnResume();

            Log.Info("myapp", "Resumed");
        }

        void captureTiming(string fullPath, double elapsed, long nanoelapsed, int iteration) // Add line to existing test file. Specify iteration and results
        {
            StreamWriter file = new StreamWriter(fullPath, true);
            file.WriteLine(iteration + ":\t\t\t" + elapsed + "\t\t\t" + nanoelapsed);
            file.Close();
        }

        void makeVisible(string fullPath) // Make file visible to system
        {
            MediaScannerConnection.ScanFile(this, new string[] { fullPath }, null, null);
        }

        void initializeFile(string fullPath, int iterations, int size, string part) // Used to write lines to file as a header style
        {
            StreamWriter file = new StreamWriter(fullPath, false);
            file.WriteLine("This file contains the recorded timings of Performance test part 2 for Xamarin.Android when {0} a SQLite Database.", part);
            file.WriteLine("");
            file.WriteLine("Iterations performed: {0}", iterations);
            file.WriteLine("Lines written to file: {0}", size);
            file.WriteLine("");
            file.WriteLine("The app was run on a device with sdk level: {0}", Build.VERSION.Sdk);
            file.WriteLine("");
            file.WriteLine("Iteration \t Milliseconds \t\t Nanoseconds");
            file.Close();
        }

    }
}


