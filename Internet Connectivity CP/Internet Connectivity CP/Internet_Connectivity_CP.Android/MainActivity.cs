﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Net;

namespace Internet_Connectivity_CP.Droid
{
	[Activity (Label = "Internet_Connectivity_CP.Android", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
            Button myButton = FindViewById<Button>(Resource.Id.myButton);

            myButton.Click += delegate
            {
                if (isOnline())
                {
                    myButton.Text = "Connected";
                }
                else { myButton.Text = "Not Connected"; }
            };
        }
        public Boolean isOnline()
        {
            ConnectivityManager cm = (ConnectivityManager)GetSystemService(Context.ConnectivityService);
            NetworkInfo netInfo = cm.ActiveNetworkInfo;
            return netInfo != null && netInfo.IsConnectedOrConnecting;
        }
    }
}


