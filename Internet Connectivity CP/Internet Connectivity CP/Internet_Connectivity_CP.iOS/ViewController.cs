﻿using Reachability;
using SystemConfiguration;
using System;
using UIKit;


namespace Internet_Connectivity_CP.iOS
{
	public partial class ViewController : UIViewController
	{
		public ViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
        }

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

        public String isOnline()
        {
            NetworkStatus internetStatus = Reachability.Reachability.LocalWifiConnectionStatus();
            if (internetStatus == NetworkStatus.NotReachable)
            {
                return "None";
            }
            else if (internetStatus == NetworkStatus.ReachableViaCarrierDataNetwork)
            {
                return "Data";
            }
            else
            {
                return "Wifi";
            }
        }

        partial void MyButton_TouchUpInside(UIButton sender)
        {
            myButton.SetTitle((Reachability.Reachability.InternetConnectionStatus()).ToString(), UIControlState.Normal);
        }
    }
}

