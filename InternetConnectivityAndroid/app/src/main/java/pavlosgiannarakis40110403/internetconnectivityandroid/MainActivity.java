package pavlosgiannarakis40110403.internetconnectivityandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button myBtn = (Button) findViewById(R.id.myBtn);
        final TextView myTV = (TextView) findViewById(R.id.myTv);

        myBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Connectivity.isConnected(getApplicationContext()))
                {
                    myTV.setText("You are connected");
                }
                else {myTV.setText("You are not connected");}
            }
        });
    }
}
