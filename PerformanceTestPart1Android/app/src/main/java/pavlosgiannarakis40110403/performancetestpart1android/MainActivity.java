package pavlosgiannarakis40110403.performancetestpart1android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.database.sqlite.*;
import android.widget.ListView;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    // Initialize UI components
    Button btn1,btn2;
    ListView listview;
    ArrayAdapter<String> y;

    // Initialize Test parameters
    int ammount = 100000;
    int iterations = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Assign UI components to code
        btn1 = (Button) findViewById(R.id.button1);
        btn2 = (Button) findViewById(R.id.button2);
        listview = (ListView) findViewById(R.id.myList);

        // Make the second button Invisible
        btn2.setVisibility(View.INVISIBLE);

        // First button click listener
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Set file name
                final String filename = "performance1AndroidTimingsA" + ammount + "I" + iterations + "Writing" + ".txt";

                //Loop to get timing samples
                for (int x = 0; x <= iterations; x++) {
                    long startTime = System.nanoTime();
                    writeToDB();
                    long stopTime = System.nanoTime();
                    long elapsed = stopTime - startTime;

                    // If condition to write to write timings to file
                    if (x == 0)
                    {
                        initializeTimingsFile(filename,"Writing");
                    }
                    else
                    {
                        captureTimings(filename,elapsed,x);
                    }
                }

                // Confirm process complete and make second button visible.
                btn1.setText("Wrote to DB and saved timings to file.");
                btn2.setVisibility(View.VISIBLE);
            }
        });

        // Second button on click listener.
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Set file name
                final String filename = "performance1AndroidTimingsA" + ammount + "I" + iterations + "Reading" + ".txt";

                //Loop to get timing samples
                for (int x = 0; x <= iterations; x++) {
                    long startTime = System.nanoTime();
                    readFromDB(x);
                    long stopTime = System.nanoTime();
                    long elapsed = stopTime - startTime;

                    // If condition to write to write timings to file
                    if (x == 0)
                    {
                        initializeTimingsFile(filename,"Reading");
                    }
                    else
                    {
                        captureTimings(filename,elapsed,x);
                    }
                }

                //Confirm process complete
                btn2.setText("Read from DB and saved to file");
            }
        });
    }

    // Method to write to database
    public void writeToDB()
    {
        SQLiteDatabase db = this.openOrCreateDatabase("MyDB.db", Context.MODE_PRIVATE, null);
        String dropDB = "DROP TABLE IF EXISTS mytests";
        db.execSQL(dropDB);
        String sqlCreate = "CREATE TABLE IF NOT EXISTS mytests ( id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT)";
        db.execSQL(sqlCreate);
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put("data", " ");
        db.beginTransaction();
        for (int i = 0; i < ammount; i++)
        {
            String sqlInsert = "INSERT INTO mytests (data) VALUES (' ')";
            //db.rawQuery(sqlInsert, null);
            db.insert("mytests","data",values);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    // Method to read from database
    public void readFromDB(int i)
    {
        SQLiteDatabase db = this.openOrCreateDatabase("MyDB.db", Context.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM mytests", null);
        if (c!=null)
        {
            ArrayList<String> testList = new ArrayList<String>();
            if (c.moveToFirst())
            {
                do {
                    String ID = c.getString(c.getColumnIndex("id"));
                    int data = c.getInt(c.getColumnIndex("data"));
                    testList.add( ID + " " + data);
                }while (c.moveToNext());
            }

            if (i == iterations) {
                y = new ArrayAdapter<String>(this, R.layout.simplerow, testList);
                listview.setAdapter(y);
            }
        }
        c.close();
    }

    // Method to write details of test to a new file.
    public void initializeTimingsFile(String Filename, String doing)
    {
        try{
            File file = new File (Environment.getExternalStorageDirectory(), Filename);
            FileOutputStream out = new FileOutputStream(file, false);
            out.write(("This file contains saved timings for androids performance test 1." + doing + "\n\n").getBytes());
            out.write(("Iterations:\t" + iterations + "\n").getBytes());
            out.write(("Ammount:\t" + ammount + "\n").getBytes());
            out.write(("\n" + "Iteration\t\t" + "Timing\n").getBytes());
            out.flush();
            out.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // Method to write line to file containing timings and the iteration.
    public void captureTimings(String Filename, long timing, int iteration)
    {
        try{
            File file = new File (Environment.getExternalStorageDirectory(), Filename);
            FileOutputStream out = new FileOutputStream(file, true);
            out.write((iteration + "\t\t\t" + timing + "\n").getBytes());
            out.flush();
            out.close();
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
