### What is this repository for? ###

This Repository was set up in order to keep track of the progress of the development aspect of my Honours Project. 

The projects title was "Cross-Platform development with Xamarin" and it set's out to compare mobile cross-platform 
application performance with native application performance. Xamarin uses C# and has recently intergrated with Visual
Studio which is why it made it so intriguing.

The way the performance was compared was by setting out a list of perfromance tests designed to compare the speed in 
which a certain task was executed. In order for each test to go forward an application was created for each test on 
each platfrom it was intened to test which in this case was iOS and android. Essentially therefore creating three 
individual apps for each test, one in cross-platform xamarin, which ran on both iOS and Android, and one on each of
the native platforms iOS and Android.

The idea being that by comparing the times it took each identical app to do the same thing it would boil down to a 
difference in performance. The final summary was intended to sum up if Xamarin was lacking in comparison or if it 
is worth being used as an alternative to native development even if the company in question is aming to get the most 
out of the applications abilities.