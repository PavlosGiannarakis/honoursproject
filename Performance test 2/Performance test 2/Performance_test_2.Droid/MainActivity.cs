﻿using Android.App;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using Android.Media;

namespace Performance_test_2.Droid
{
	[Activity (Label = "Performance_test_2.Droid", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
        int iterations = 100; //Set iteration number
        int size = 100000; //Set number of lines written to file
        ListView listview; //Initiate listview element 

        protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Connect UI elements to code.
			Button button = FindViewById<Button> (Resource.Id.myButton);
            Button button2 = FindViewById<Button>(Resource.Id.button1);
            listview = FindViewById<ListView>(Resource.Id.listView1);
            //Make the second button invisible.
            button2.Visibility = ViewStates.Invisible;

            button.Click += delegate {
                // Next 3 Lines set the file name and create the path to it.
                string filename = string.Format("XamarinAndroidTimingsFileWrite{0}X{1}.txt", size,iterations);
                var pathDir = global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
                var fullPath = Path.Combine(pathDir.ToString(), "Timings", filename);

                //Loop for saving timings to file
                for (int i = 0; i <= iterations; i++)
                {
                    Stopwatch stopWatch1 = new Stopwatch(); // Create stopwatch instance

                    stopWatch1.Start(); // Start the stopwatch
                    var start = Java.Lang.JavaSystem.NanoTime(); // Capture java system nano time for start

                    writeToFile();

                    var end = Java.Lang.JavaSystem.NanoTime(); // Capture java system nano time for end
                    stopWatch1.Stop(); // Stop the stopwatch

                    double elapsed = stopWatch1.Elapsed.Milliseconds; // Convert stopwatch elapsed to Milliseconds
                    var nanoelapsed = end - start; // Get elapsed Java nano time

                    // If condition sets the file and captures the timings
                    if (i != 0)
                    {
                        captureTiming(fullPath, elapsed, nanoelapsed, i); // Capture timing and save to file
                    }
                    else
                    {
                        initializeFile(fullPath, iterations, size, "writing to"); // Initialize file
                    }
                }
                button.Text = string.Format ("Wrote to file and got timings"); // Let the user know that the operation is complete by changing the button text
                makeVisible(fullPath); // Make the timings file visible in system.
                button2.Visibility = ViewStates.Visible; // Make the second button visible
            };

            button2.Click += delegate
            {
                // Next 3 Lines set the file name and create the path to it.
                string filename = string.Format("XamarinAndroidTimingsFileRead{0}X{1}.txt", size, iterations);
                var pathDir = global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
                var fullPath = Path.Combine(pathDir.ToString(), "Timings", filename);
                
                //Loop for saving timings to file
                for (int i = 0; i <= iterations; i++)
                {
                    Stopwatch stopWatch1 = new Stopwatch(); // Create stopwatch instance
                    stopWatch1.Start(); // Start the stopwatch
                    var start = Java.Lang.JavaSystem.NanoTime(); // Get current Java nanotime 

                    readFromFile(i); // Read from the file method

                    var end = Java.Lang.JavaSystem.NanoTime(); // Get current Java nanotime
                    stopWatch1.Stop();

                    
                    double elapsed = stopWatch1.Elapsed.Milliseconds; // Get elapsed time from stopwatch and convert it to Milliseconds
                    var nanoelapsed = end - start; // Calculate java nanotime elapsed.
                    if (i != 0) // If condition to store timings to file
                    {
                        captureTiming(fullPath, elapsed, nanoelapsed, i); // Save timing method
                    }
                    else
                    {
                        initializeFile(fullPath, iterations, size, "reading from"); // If first loop, discard timing and instead initialize file
                    }
                }
                makeVisible(fullPath); // Make timings file visible to host system. 
                button2.Text = "Read from file"; // Confirm Actions have been completed
            };
		}

        void writeToFile() // Method used to write to file
        {
            string[] lines = new string[size]; // Create new array of strings of size "size"
            for (int i = 0; i < size; i++) // Loop to fill the array
            {
                lines[i] = (i+1).ToString();
            }

            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string filename = Path.Combine(path, "myTestfile.txt"); // Get path to file

            using (var streamWriter = new StreamWriter(filename, false)) // Creates the file if it doesn't exist and opens a writer to it
            {
                foreach (string line in lines) // Write to file line by line
                {
                    streamWriter.WriteLine(line);
                }
                // Close?
            }
        }

        void readFromFile(int i)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string filename = Path.Combine(path, "myTestfile.txt"); // Get file path which was created when writing // Same?
            string line; // Initialize an empty line
            
            List<string> myItems = new List<string>(); // List of strings

            ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, myItems); //Array adapter for listview
            using (var streamReader = new StreamReader(filename)) // Create reader for file at the path given
            {
                while ((line = streamReader.ReadLine()) != null) // While the line isn't empty read file to List of strings
                {
                    myItems.Add(line);
                }
            }

            if (i == iterations)
            {
                adapter.AddAll(myItems); // Add items from list of strings to Array adapter
                listview.Adapter = adapter; // Assign created adapter to listview
            }
        }

        void captureTiming(string fullPath, double elapsed, long nanoelapsed, int iteration) // Mehtod that takes the path,timings and iteration and stores it to the file
        {
            StreamWriter file = new StreamWriter(fullPath, true);
            file.WriteLine(iteration + ":\t\t\t" + elapsed + "\t\t\t" + nanoelapsed);
            file.Close();
        }

        void makeVisible(string fullPath) // Use method to make file created visible to android system
        {
            MediaScannerConnection.ScanFile(this, new string[] { fullPath }, null, null);
        }

        void initializeFile(string fullPath, int iterations, int size, string part) // Takes the data at the start of and uses it to create a header for the file
        {                                                                           // While also initializing it.
            StreamWriter file = new StreamWriter(fullPath, false);
            file.WriteLine("This file contains the recorded timings of Performance test part 2 for Xamarin.Android when {0} a file.", part);
            file.WriteLine("");
            file.WriteLine("Iterations performed: {0}", iterations);
            file.WriteLine("Lines written to file: {0}", size);
            file.WriteLine("");
            file.WriteLine("The app was run on a device with sdk level: {0}", Build.VERSION.Sdk);
            file.WriteLine("");
            file.WriteLine("Iteration \t Milliseconds \t\t Nanoseconds");
            file.Close();
        }
    }
}


