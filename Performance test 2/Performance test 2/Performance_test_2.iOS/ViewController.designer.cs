﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Performance_test_2.iOS
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Reed { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView table { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Write { get; set; }

        [Action ("Reed_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void Reed_TouchUpInside (UIKit.UIButton sender);

        [Action ("Write_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void Write_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (Reed != null) {
                Reed.Dispose ();
                Reed = null;
            }

            if (table != null) {
                table.Dispose ();
                table = null;
            }

            if (Write != null) {
                Write.Dispose ();
                Write = null;
            }
        }
    }
}