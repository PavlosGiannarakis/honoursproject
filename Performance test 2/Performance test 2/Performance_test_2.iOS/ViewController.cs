﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UIKit;

namespace Performance_test_2.iOS
{
	public partial class ViewController : UIViewController
	{
		public ViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		}

        public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

        //Set tests parameters
        int iterations = 100;
        int size = 100;

        //On click of write button
        partial void Write_TouchUpInside(UIButton sender)
        {
            //Set name for timings file
            string filename = string.Format("XamariniOSTimingsFileWrite{0}R{1}.txt", size, iterations);
            var pathDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var fullPath = Path.Combine(pathDir.ToString(), filename);
            
            //Loop for gathering the timings
            for (int i = 0; i <= iterations; i++)
            {
                Stopwatch stopWatch1 = new Stopwatch(); // Create stopwatch
                stopWatch1.Start(); // Start the stopwatch
                
                writeToFile(); // Run the method to write to file

                stopWatch1.Stop(); // Stop the stopwatch

                double elapsed = stopWatch1.Elapsed.Milliseconds; // Convert elapsed time to Milliseconds
                
                //Set file with test parameters and then add a line of test results for each iteration.
                if (i != 0)
                {
                    captureTiming(fullPath, elapsed, i);
                }
                else
                {
                    initializeFile(fullPath, iterations, size, "writing to");
                }
            }
            //Confirm process is complete
            var newtitle = "Wrote to file and saved timings";
            Write.SetTitle(newtitle, UIControlState.Normal);
        }

        //Read button click
        partial void Reed_TouchUpInside(UIButton sender)
        {
            //Set name for file of testing outputs
            string filename = string.Format("XamariniOSTimingsFileRead{0}R{1}.txt", size, iterations);
            var pathDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var fullPath = Path.Combine(pathDir.ToString(), filename);

            //Loop for getting multiple testing samples
            for (int i = 0; i <= iterations; i++)
            {
                Stopwatch stopWatch1 = new Stopwatch(); // Create stopwatch
                stopWatch1.Start(); // Start the stopwatch

                readFromFile(i); // Run method

                stopWatch1.Stop(); //Stop the stopwatch

                double elapsed = stopWatch1.Elapsed.Milliseconds; // Convert elapsed time to Milliseconds
                
                //Set file with test parameters and then add timings, iteration for each loop
                if (i != 0)
                {
                    captureTiming(fullPath, elapsed, i);
                }
                else
                {
                    initializeFile(fullPath, iterations, size, "reading from");
                }
            }

            //Confirm process complete
            var newtitle = "Read data from the file and saved timings";
            Reed.SetTitle(newtitle, UIControlState.Normal);
        }

        //Method to write to file
        void writeToFile()
        {
            string[] lines = new string[size];
            for (int i = 0; i < size; i++)
            {
                lines[i] = (i + 1).ToString();
            }

            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string filename = Path.Combine(path, "myfile.txt");
            Debug.Write(filename);

            using (var streamWriter = new StreamWriter(filename, false))
            {
                streamWriter.Write(lines);// Works? No line for iOS because then it needs objective c code.
                foreach (string line in lines)
                {
                    streamWriter.WriteLine(line);
                }
            }
        }

        // Method to read from file
        void readFromFile(int iteration)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string filename = Path.Combine(path, "myfile.txt");
            string line;

            List<string> myItems = new List<string>();

            using (var streamReader = new StreamReader(filename))
            {
                while ((line = streamReader.ReadLine()) != null && line != "")
                {
                    myItems.Add(line);
                }

            }
            if (iteration == iterations)
            {
                table.Source = new TableSource(myItems.ToArray());
                table.ReloadData();
            }

        }

        // Add timing and iteration to file
        void captureTiming(string fullPath, double elapsed, int iteration)
        {
            StreamWriter file = new StreamWriter(fullPath, true);
            file.WriteLine(iteration + ":\t\t\t" + elapsed);
            file.Close();
        }

        //Initialize file by adding details about test and its parameters
        void initializeFile(string fullPath, int iterations, int size, string part)
        {
            StreamWriter file = new StreamWriter(fullPath, false);
            file.WriteLine("This file contains the recorded timings of Performance test part 2 for Xamarin.iOS when {0} a file.", part);
            file.WriteLine("");
            file.WriteLine("Iterations performed: {0}", iterations);
            file.WriteLine("Lines written to file: {0}", size);
            file.WriteLine("");
            file.WriteLine("Iteration \t Milliseconds ");
            file.Close();
        }

    }
}

