package pavlosgiannarakis40110403.performancetestpart2android;

import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InvalidObjectException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    //SET UI Components code elements
    Button writeBtn;
    Button readBtn;
    ListView listview;

    //Initialize test parameters
    int ammount = 100000;
    int iterations = 100;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize UI Code elements
        writeBtn = (Button) findViewById(R.id.writeBtn);
        readBtn = (Button) findViewById(R.id.readBtn);
        listview = (ListView) findViewById(R.id.myList);

        //Make the second button invisible
        readBtn.setVisibility(View.INVISIBLE);

        //On click listener for write button
        writeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //File name
                final String filename = "performance2WriteAndroidTimingsA" + ammount + "I" + iterations + "Writing" + ".txt";
                // Loop for getting timing samples
                for (int x = 0; x <= iterations; x++) {
                    long startTime = System.nanoTime();
                    writeToFile(ammount);
                    long stopTime = System.nanoTime();
                    long elapsed = stopTime - startTime;

                    // Write samples to file
                    if (x == 0) {
                        initializeTimingsFile(filename, "Writing");
                    } else {
                        captureTimings(filename, elapsed, x);
                    }
                }
                //Confirm process complete and set second button visible
                writeBtn.setText("Wrote to file and saved timings");
                readBtn.setVisibility(View.VISIBLE); // Make read button
            }
        });

        //On click listener for read button
        readBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //File name
                final String filename = "performance2ReadAndroidTimingsA" + ammount + "I" + iterations + "Writing" + ".txt";
                // Loop for getting timing samples
                for (int x = 0; x <= iterations; x++) {
                    long startTime = System.nanoTime();
                    readFromFile(x);
                    long stopTime = System.nanoTime();
                    long elapsed = stopTime - startTime;

                    // Write samples to file
                    if (x == 0) {
                        initializeTimingsFile(filename, "reading");
                    } else {
                        captureTimings(filename, elapsed, x);
                    }
                }
                //Confirm process complete
                readBtn.setText("Read from file and saved timings");
            }
        });
    }

    // Set top of file with details about test
    public void initializeTimingsFile(String Filename, String doing)
    {
        try{
            File file = new File (Environment.getExternalStorageDirectory(), Filename);
            FileOutputStream out = new FileOutputStream(file, false);
            out.write(("This file contains saved timings for androids performance test 1." + doing + "\n\n").getBytes());
            out.write(("Iterations:\t" + iterations + "\n").getBytes());
            out.write(("Ammount:\t" + ammount + "\n").getBytes());
            out.write(("\n" + "Iteration\t\t" + "Timing\n").getBytes());
            out.flush();
            out.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // Write line to file containing timings and iteration
    public void captureTimings(String Filename, long timing, int iteration)
    {
        try{
            File file = new File (Environment.getExternalStorageDirectory(), Filename);
            FileOutputStream out = new FileOutputStream(file, true);
            out.write((iteration + "\t\t\t" + timing + "\n").getBytes());
            out.flush();
            out.close();
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // Method to write to file, loop until limit and write line to file each time
    public void writeToFile(int limit)
    {
        try {
            String filename = "myfile";
            FileOutputStream fos = openFileOutput(filename, Context.MODE_PRIVATE);
            for(int i = 0; i < limit; i++) {
                fos.write((String.valueOf(i+1) + "\n").getBytes());
            }
            fos.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    //Method to read from file, while lines are available read line from file
    public void readFromFile(int i)
    {
        ArrayList<String> testList = new ArrayList<String>();
        try
        {
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(openFileInput("myfile")));
            String inputString;
            while((inputString = inputReader.readLine()) != null)
            {
                testList.add(inputString);
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        if (i == iterations) {
            try {
                ArrayAdapter y = new ArrayAdapter<>(this, R.layout.simplerow, testList);
                listview.setAdapter(y);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}
