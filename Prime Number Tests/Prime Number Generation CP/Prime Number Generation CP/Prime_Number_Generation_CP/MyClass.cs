﻿namespace Prime_Number_Generation_CP
{
    public class MyClass
    {
        public MyClass()
        {

        }

        // Find primes using sieve of eratosthenes
        public bool[] FindPrimes(int max)
        {
            // Make an array indicating whether numbers are prime.
            bool[] is_prime = new bool[max + 1];
            for (int i = 2; i <= max; i++) is_prime[i] = true;

            // Cross out multiples.
            for (int i = 2; i <= max; i++)
            {
                // See if i is prime.
                if (is_prime[i])
                {
                    // Knock out multiples of i.
                    for (int j = i * 2; j <= max; j += i)
                        is_prime[j] = false;
                }
            }
            return is_prime;
        }

        //Count primes by checking which positions are true
        public int countPrimes(bool[] x)
        {
            int count = 0;
            for (int i = 2; i < x.Length; i++)
            {
                if (x[i])
                {
                    count++;
                }
            }
            return count;
        }

        //Get list of primes by checking which positions are true
        public int[] getPrimes(bool[] x, int size)
        {
            int[] result = new int[size + 1];
            int count = -1;
            for (int i = 2; i < x.Length; i++)
            {
                if (x[i])
                {
                    count++;
                    result[count] = i;
                }
            }
            return result;
        }
    }
}

