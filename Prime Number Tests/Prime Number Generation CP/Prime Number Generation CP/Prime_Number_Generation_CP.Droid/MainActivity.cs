﻿using System.IO;
using System.Diagnostics;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Media;

namespace Prime_Number_Generation_CP.Droid
{
	[Activity (Label = "Prime_Number_Generation_CP.Droid", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
        int iterations = 1; // Set the ammount of iteratons for the samples taken
        int limit = 100000; // Sets the limit to search for primes to.

        protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button> (Resource.Id.myButton); // Connect UI Button to code.
            MyClass x = new MyClass(); // Create instance of portable class library.
            bool[] primeList; // Create a list of boolean values.

            //Next 3 Lines are used to get the path to the file
            string filename = string.Format("XamarinAndroidTimingsPrimes{0}X{1}.txt", limit, iterations);
            var pathDir = global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            var fullPath = Path.Combine(pathDir.ToString(), "Timings", filename);


            button.Text = "Click to start";// Set button text.
            button.Click += delegate // Set Button response to click.
            {
                Stopwatch stopWatch = new Stopwatch(); // Create stopwatch instance.
                for (int i = 0; i <= iterations; i++)
                {
                    stopWatch.Reset();
                    stopWatch.Start();  // Start stopwatch
                    var start = Java.Lang.JavaSystem.NanoTime(); // Capture Java system nano time

                    primeList = x.FindPrimes(limit);
                    
                    var end = Java.Lang.JavaSystem.NanoTime(); // Capture Java system nano time
                    stopWatch.Stop();   // Stop stopwatch
                    var nanoelapsed = end - start; // Calculate elapsed time in java system nano time.
                    double elapsed = stopWatch.Elapsed.Milliseconds; // Get elapsed time from stopwatch.
                    if (i != 0)
                    {
                        captureTiming(fullPath, elapsed, nanoelapsed, i); // Used to store timings and iteration nummber as a line to file.
                        if (i == iterations)
                        {
                            endfile(fullPath, x.countPrimes(primeList));
                        }
                    }
                    else
                    {
                        initializeFile(fullPath, iterations, limit); // Used to initialize file and discard the first timing.
                    }
                }

                makeVisible(fullPath); // Make file visible to system
                button.Text = "Timings where saved to file."; // Confirm procedure is complete.
            };
        }

        void captureTiming(string fullPath, double elapsed, long nanoelapsed, int iteration) // Create a stream writer that allows you to append a line to the file.
        {
            StreamWriter file = new StreamWriter(fullPath, true);
            file.WriteLine(iteration + ":\t\t\t" + elapsed + "\t\t\t" + nanoelapsed);
            file.Close();
        }

        // Add result to the end of the file.
        void endfile(string fullPath, int count) 
        {
            StreamWriter file = new StreamWriter(fullPath, true);
            file.WriteLine("");
            file.WriteLine("In this test " + count + " primes where found");
            file.Close();
        }

        void makeVisible(string fullPath) // Make file Visible
        {
            MediaScannerConnection.ScanFile(this, new string[] { fullPath }, null, null);
        }

        void initializeFile(string fullPath, int iterations, int limit) // Create a stream writer that allows you to recreate the file from scratchho
        {
            StreamWriter file = new StreamWriter(fullPath, false);
            file.WriteLine("This file contains the recorded timings of Prime number generation for Xamarin.Android with a limit of {0}", limit);
            file.WriteLine("");
            file.WriteLine("Iterations performed: {0}", iterations);
            file.WriteLine("The maximum number set is: {0}", limit);
            file.WriteLine("");
            file.WriteLine("The app was run on a device with sdk level: {0}", Build.VERSION.Sdk);
            file.WriteLine("");
            file.WriteLine("Iteration \t Milliseconds \t\t Nanoseconds");
            file.Close();
        }
    }
}


