﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Prime_Number_Generation_CP.iOS
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Main_button { get; set; }

        [Action ("Main_button_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void Main_button_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (Main_button != null) {
                Main_button.Dispose ();
                Main_button = null;
            }
        }
    }
}