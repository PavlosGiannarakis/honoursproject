﻿using System;
using System.Diagnostics;
using System.IO;
using UIKit;

namespace Prime_Number_Generation_CP.iOS
{
	public partial class ViewController : UIViewController
	{
        //set test parameters
		bool[] primesList;
        int limit = 10000000;
        int iterations = 100;

        public ViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
            // Perform any additional setup after loading the view, typically from a nib.
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

        partial void Main_button_TouchUpInside(UIButton sender)
        {
            //Set name for file of testing outputs
            string filename = string.Format("XamariniOSTimingsPrimes{0}R{1}.txt", limit, iterations);
            var pathDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var fullPath = Path.Combine(pathDir.ToString(), filename);

            //Create instance of pcl class and stopwatch
            MyClass x = new MyClass();
            Stopwatch stopWatch = new Stopwatch();

            //Loop to gather timings
            for (int i = 0; i <= iterations; i++)
            {
                //Reset stopwatch and start it
                stopWatch.Reset();
                stopWatch.Start();
                //Run method
                primesList = x.FindPrimes(limit);
                //Stop stopwatch
                stopWatch.Stop();

                //Calculate elapsed milliseconds
                double elapsed = stopWatch.Elapsed.Milliseconds;

                //Set file with test parameters and then add timings, iteration for each loop
                if (i != 0)
                {
                    captureTiming(fullPath, elapsed, i);
                    if (i == iterations)
                    {
                        endfile(fullPath, x.countPrimes(primesList));
                    }
                }
                else
                {
                    initializeFile(fullPath, iterations, limit);
                }
            }

            var title = "Times saved to file.";
            Main_button.SetTitle(title, UIControlState.Normal);
        }

        // Add timing and iteration to file
        void captureTiming(string fullPath, double elapsed, int iteration)
        {
            StreamWriter file = new StreamWriter(fullPath, true);
            file.WriteLine(iteration + ":\t\t\t" + elapsed);
            file.Close();
        }

        // Add result to the end of the file.
        void endfile(string fullPath, int count)
        {
            StreamWriter file = new StreamWriter(fullPath, true);
            file.WriteLine("");
            file.WriteLine("In this test " + count + " primes where found");
            file.Close();
        }

        //Initialize file by adding details about test and its parameters
        void initializeFile(string fullPath, int iterations, int limit)
        {
            StreamWriter file = new StreamWriter(fullPath, false);
            file.WriteLine("This file contains the recorded timings of prime number generation for Xamarin.iOS.");
            file.WriteLine("");
            file.WriteLine("Iterations performed: {0}", iterations);
            file.WriteLine("Limit for primes: {0}", limit);
            file.WriteLine("");
            file.WriteLine("Iteration \t Milliseconds ");
            file.Close();
        }
    }
}

