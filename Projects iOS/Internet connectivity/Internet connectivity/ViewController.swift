//
//  ViewController.swift
//  Internet connectivity
//
//  Created by SchoolofComputing on 19/03/2017.
//  Copyright © 2017 Pavlos Giannarakis. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration

class ViewController: UIViewController {
    @IBOutlet weak var button1: UIButton!
    
    @IBAction func btn_click(_ sender: Any) {
        if (isInternetAvailable())
        {
            button1.setTitle("Internet is available", for: UIControlState.normal)
        }
        else
        {
            button1.setTitle("No Internet is available", for: UIControlState.normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }


}

