//
//  ViewController.swift
//  Prime Numbers iOS
//
//  Created by SchoolofComputing on 28/02/2017.
//  Copyright © 2017 Pavlos Giannarakis. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    //Set parameters of tests
    var limit = 10000000
    let iterations = 100
    
    //Map UI elements to code to code and and click function
    @IBOutlet weak var primeBtn: UIButton!
    @IBAction func retrievePrimes(_ sender: Any) {
        //Set file name for testing and saving testing times
        let timingFile = "iOSTimingPrimes\(limit)R\(iterations).txt"
        var primeList : [Bool] = []
        
        // Loop to get sample timings
        for j in 0...iterations
        {
            //Start timing, run function, stop timing and calculate elapsed time
            let t1 = DispatchTime.now()
            primeList = findPrimes(max: limit)
            let t2 = DispatchTime.now()
            let elapsed = t2.uptimeNanoseconds - t1.uptimeNanoseconds
            //Set file with test parameters and then add a timing and iteration for each iteration
            if j == 0
            {
                setFile(path: timingFile)
            }
            else if j == iterations
            {
                saveTimings(path: timingFile, iteration: j, nanoelapsed: elapsed)
                let count = countPrimes(x: primeList)
                endFile(path: timingFile, count: count)
            }
            else
            {
                saveTimings(path: timingFile, iteration: j, nanoelapsed: elapsed)
            }
        }
        
        let newTitle = "Process complete"
        primeBtn.setTitle(newTitle, for: UIControlState.normal)
    }
    
    //find primes using sieve of eratothenes
    func findPrimes(max : Int) -> [Bool]
    {
        var is_prime = [Bool](repeating: true, count:max+1)
        is_prime[0] = false
        is_prime[1] = false
        
        for i in 2...max
        {
            if(is_prime[i])
            {
                for j in stride(from: i*2, to: max + 1, by: i)
                {
                    is_prime[j] = false
                }
            }
        }
        return is_prime
    }
    
    //Count primes
    func countPrimes(x : [Bool]) -> Int
    {
        var count = 0
        for i in 2...(x.count-1)
        {
            if (x[i])
            {
                count += 1
            }
        }
        return count
    }
    
    //Get list of primes
    func getPrimes(x : [Bool]) -> [String]
    {
        var result : [String] = []
        var count = -1
        for i in 2...(x.count-1)
        {
            if (x[i])
            {
                count += 1
                result[count] = String(i)
            }
        }
        
        return result
    }
    
    // Initiliaze file with test parameters
    func setFile(path: String) -> Void
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let timingPath = dir.appendingPathComponent(path)
            let greeting = "This file contains the recorded timings of prime number generation iOS\n\n"
            let data = "Iterations performed: \(iterations)\n"
            let data2 = "Limit for primes: \(limit)\n\n"
            let layout = "Iteration \t\t\t Nanoseconds \n"
            if let outputStream = OutputStream(url: timingPath, append: false)
            {
                outputStream.open()
                outputStream.write(greeting, maxLength: greeting.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.write(data, maxLength: data.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.write(data2, maxLength: data2.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.write(layout, maxLength: layout.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.close()
            }
        }
    }
    
    //Append a line to file with timings
    func saveTimings(path: String, iteration: Int,nanoelapsed: UInt64) -> Void
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let timingPath = dir.appendingPathComponent(path)
            let timing = "\t\(iteration) \t\t\t \(nanoelapsed) \n"
            if let outputStream = OutputStream(url: timingPath, append: true)
            {
                outputStream.open()
                outputStream.write(timing, maxLength: timing.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.close()
            }
        }
    }
    
    //Function to add test results to end of file
    func endFile(path: String, count: Int) -> Void
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let endingPath = dir.appendingPathComponent(path)
            let ending = "\n The prime count for this test is \(count)"
            if let outputStream = OutputStream(url: endingPath, append: true)
            {
                outputStream.open()
                outputStream.write(ending, maxLength: ending.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.close()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

