//
//  ViewController.swift
//  Performance test 1 iOS
//
//  Created by SchoolofComputing on 11/03/2017.
//  Copyright © 2017 Pavlos Giannarakis. All rights reserved.
//

import UIKit
import SQLite

class ViewController: UIViewController {
    
    //Set UI components
    @IBOutlet weak var writeBtn: UIButton!
    @IBOutlet weak var readBtn: UIButton!
    @IBOutlet weak var tableview: UITableView!
    
    //Set test parameters
    var iterations = 100
    var lines = 100
    
    //On click handler for writing to db
    @IBAction func write_to_db(_ sender: Any) {
    }
    
    //On click handler for reading db
    @IBAction func read_from_db(_ sender: Any) {
    }
    
    // Initiliaze file with test parameters
    func setFile(path: String, part: String) -> Void
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let timingPath = dir.appendingPathComponent(path)
            let greeting = "This file contains the recorded timings of performance test 1 when \(part) a database \n\n"
            let data = "Iterations performed: \(iterations)\n"
            let data2 = "Lines written to database: \(size)\n\n"
            let layout = "Iteration \t\t\t Milliseconds \t\t\t Nanoseconds \n"
            if let outputStream = OutputStream(url: timingPath, append: false)
            {
                outputStream.open()
                outputStream.write(greeting, maxLength: greeting.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.write(data, maxLength: data.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.write(data2, maxLength: data2.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.write(layout, maxLength: layout.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.close()
            }
        }
    }
    
    //Append a line to file with timings
    func saveTimings(path: String, iteration: Int,nanoelapsed: UInt64, millielapsed: UInt64) -> Void
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let timingPath = dir.appendingPathComponent(path)
            let timing = "\t\(iteration) \t\t\t\t \(millielapsed) \t\t\t \(nanoelapsed) \n"
            if let outputStream = OutputStream(url: timingPath, append: true)
            {
                outputStream.open()
                outputStream.write(timing, maxLength: timing.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.close()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        let line = lines[indexPath.row]
        cell.textLabel?.text = String(line)
        
        return cell
    }


}

