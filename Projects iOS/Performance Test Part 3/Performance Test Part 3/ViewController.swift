//
//  ViewController.swift
//  Performance Test Part 3
//
//  Created by SchoolofComputing on 19/03/2017.
//  Copyright © 2017 Pavlos Giannarakis. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var findBtn: UIButton!
    @IBOutlet weak var replaceBtn: UIButton!
    @IBOutlet weak var tv1: UITextField!
    @IBOutlet weak var tv2: UITextField!
    
    
    
    //Set iteration rate
    var iterations = 100
    
    @IBAction func findBtnClick(_ sender: Any) {
        let timingFile = "iOSTimingPT3finding\(iterations).txt"
        // Loop to get sample timings
        for j in 0...iterations
        {
            //Start timing, run function, stop timing and calculate elapsed time
            let t1 = DispatchTime.now()
            let count1 = count()
            let t2 = DispatchTime.now()
            let elapsed = t2.uptimeNanoseconds - t1.uptimeNanoseconds
            //Set file with test parameters and then add a timing and iteration for each iteration
            if j == 0
            {
                setFile(path: timingFile)
            }
            else
            {
                saveTimings(path: timingFile, iteration: j, nanoelapsed: elapsed)
            }
        }
        
    }
    
    @IBAction func replaceBtnClick(_ sender: Any) {
        let timingFile = "iOSTimingPT3replacing\(iterations).txt"
        // Loop to get sample timings
        for j in 0...iterations
        {
            //Start timing, run function, stop timing and calculate elapsed time
            let t1 = DispatchTime.now()
            let fileUrl = Bundle.main.url(forResource: "file", withExtension: "txt")
            if try! fileUrl!.checkResourceIsReachable() == true
            {
                let con = try! String(contentsOf: fileUrl!, encoding: String.Encoding.ascii)
                let con2 = con.replacingOccurrences(of: tv1.text!, with: tv2.text!)
            }
            let t2 = DispatchTime.now()
            let elapsed = t2.uptimeNanoseconds - t1.uptimeNanoseconds
            //Set file with test parameters and then add a timing and iteration for each iteration
            if j == 0
            {
                setFile(path: timingFile)
            }
            else
            {
                saveTimings(path: timingFile, iteration: j, nanoelapsed: elapsed)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Load file and count instances of word
    func count() -> Int
    {
        var count = 0
        let fileUrl = Bundle.main.url(forResource: "file", withExtension: "txt")
        if try! fileUrl!.checkResourceIsReachable() == true
        {
            let con = try! String(contentsOf: fileUrl!, encoding: String.Encoding.ascii)
            let stringArray = con.components(separatedBy: CharacterSet.punctuationCharacters).joined()
            let stringArray2 = stringArray.components(separatedBy: CharacterSet.newlines).joined()
            let words = stringArray2.components(separatedBy: " ")
            
            for var word in words
            {
                if word.caseInsensitiveCompare(tv1.text!) == ComparisonResult.orderedSame
                {
                    count += 1
                }
            }
        }
        else{print("not")}
        return count
    }
    
    // Initiliaze file with test parameters
    func setFile(path: String) -> Void
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let timingPath = dir.appendingPathComponent(path)
            let greeting = "This file contains the recorded timings of Performance test 3 iOS\n\n"
            let data = "Iterations performed: \(iterations)\n"
            let layout = "Iteration \t\t\t Nanoseconds \n"
            if let outputStream = OutputStream(url: timingPath, append: false)
            {
                outputStream.open()
                outputStream.write(greeting, maxLength: greeting.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.write(data, maxLength: data.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.write(layout, maxLength: layout.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.close()
            }
        }
    }
    
    //Append a line to file with timings
    func saveTimings(path: String, iteration: Int,nanoelapsed: UInt64) -> Void
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let timingPath = dir.appendingPathComponent(path)
            let timing = "\t\(iteration) \t\t\t \(nanoelapsed) \n"
            if let outputStream = OutputStream(url: timingPath, append: true)
            {
                outputStream.open()
                outputStream.write(timing, maxLength: timing.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.close()
            }
        }
    }
    
}
