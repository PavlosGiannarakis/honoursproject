//
//  ViewController.swift
//  Performace test 2
//
//  Created by SchoolofComputing on 11/03/2017.
//  Copyright © 2017 Pavlos Giannarakis. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //Map UI Components to code
    @IBOutlet weak var writeBtn: UIButton!
    @IBOutlet weak var readBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    //Initialize test variables and parameters
    var lines : [String] = []
    var tempLines : [String] = []
    var iterations = 100
    var size = 1000000
    
    //On click function run when write button is clicked
    @IBAction func write_to_file(_ sender: Any) {
        //Set file names for testing and saving testing times
        let file =  "file.txt"
        let timingFile = "iOSTimingFileWrite\(size)R\(iterations).txt"
        
        // Loop to get sample timings
        for j in 0...iterations
        {
            // Get directory
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            {
                //start timing
                let start = DispatchTime.now()
                
                //Create File and array of string to write to it
                let path = dir.appendingPathComponent(file)
                var array = [String]()
                
                //Write to array
                for i in 0...size
                {
                    let x = "\(i)"
                    array.append(x)
                }
                // Write array to file
                do{
                    let stringArray = array.joined(separator: "\n")
                    try stringArray.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                }
                catch
                {
                }
                
                //Stop timing
                let end = DispatchTime.now()
                
                //Calculate nanotime and milliseconds
                let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
                let milliTime = nanoTime/1000000
            
            //Write to timings file, by setting it and then adding a timing for each iteration
            if j == 0
            {
                setFile(path: timingFile, part: "writing to")
            }
            else
            {
                saveTimings(path: timingFile, iteration: j, nanoelapsed: nanoTime, millielapsed: milliTime)
            }
            }
        }
        //Confirm process complete
        writeBtn.setTitle("Wrote to file and saved times", for: UIControlState.normal)
    }
    
    @IBAction func read_from_file(_ sender: Any) {
        //Set file names
        let file =  "file.txt"
        let timingFile = "iOSTimingFileRead\(size)R\(iterations).txt"
        
        //Loop to gather sample timings
        for j in 0...iterations
        {
            //Start timing
            let start = DispatchTime.now()
            
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            {
                let path = dir.appendingPathComponent(file)
                do{
                    // Get file as string and split it to array of string seperated by new lines.
                    let myFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
                    tempLines = myFile.components(separatedBy: .newlines)
                    
                    // Add data to table on last iteration
                    if (j == iterations)
                    {
                        lines = tempLines
                        tableView.reloadData()
                    }
                }
                catch
                {
                    
                }
                
            }
            
            //Stop timing and calculate elapsed time in milli and nano seconds.
            let end = DispatchTime.now()
            let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
            let milliTime = nanoTime/1000000
            
            //Set file with test parameters and then add a timing and iteration for each iteration
            if j == 0
            {
                setFile(path: timingFile, part: "reading from ")
            }
            else
            {
                saveTimings(path: timingFile, iteration: j, nanoelapsed: nanoTime, millielapsed: milliTime)
            }
        }
        
        // Confirm process complete
        readBtn.setTitle("Read from file and saved times", for: UIControlState.normal)
    }
    
    // Initiliaze file with test parameters
    func setFile(path: String, part: String) -> Void
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let timingPath = dir.appendingPathComponent(path)
            let greeting = "This file contains the recorded timings of performance test 2 when \(part) a file \n\n"
            let data = "Iterations performed: \(iterations)\n"
            let data2 = "Lines written to file: \(size)\n\n"
            let layout = "Iteration \t\t\t Milliseconds \t\t\t Nanoseconds \n"
            if let outputStream = OutputStream(url: timingPath, append: false)
            {
                outputStream.open()
                outputStream.write(greeting, maxLength: greeting.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.write(data, maxLength: data.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.write(data2, maxLength: data2.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.write(layout, maxLength: layout.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.close()
            }
        }
    }
    
    //Append a line to file with timings
    func saveTimings(path: String, iteration: Int,nanoelapsed: UInt64, millielapsed: UInt64) -> Void
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let timingPath = dir.appendingPathComponent(path)
            let timing = "\t\(iteration) \t\t\t\t \(millielapsed) \t\t\t \(nanoelapsed) \n"
            if let outputStream = OutputStream(url: timingPath, append: true)
            {
                outputStream.open()
                outputStream.write(timing, maxLength: timing.lengthOfBytes(using: String.Encoding.utf8))
                outputStream.close()
            }
        }
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        let line = lines[indexPath.row]
        cell.textLabel?.text = String(line)
        
        return cell
    }
    
}
