package pavlosgiannarakis40110403.performancetestpart3;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {
    // Initialize UI Components
    EditText myET1;
    EditText myET2;
    Button myFind;
    Button myReplace;

    // Initialize variables need for test
    String searchex;
    int iterations = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Map UI components to code
        myET1 = (EditText) findViewById(R.id.editText);
        myET2 = (EditText) findViewById(R.id.editText2);
        myFind = (Button) findViewById(R.id.btn1);
        myReplace = (Button) findViewById(R.id.btn2);

        //Set Asset Manager to read file from assets as string, Method to read as string created.
        AssetManager am = this.getAssets();
        searchex = getAssetAsString(am);

        // Click listner for button to find
        myFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Set output file name
                final String filename = "performance3AndroidTimingsI" + iterations + "Finding" + ".txt";

                // Loop to get timing samples
                for (int x = 0; x <= iterations; x++) {
                    // Start timing
                    long startTime = System.nanoTime();

                    // Sets integer equal to the number instances of the word found
                    int count = exists();

                    //Stop timing
                    long stopTime = System.nanoTime();
                    // Calculate elapsed time
                    long elapsed = stopTime - startTime;

                    if (x == 0) // Initialize and write samples to file, when complete add results.
                    {
                        initializeTimingsFile(filename, "Search");
                    }
                    else if (x == iterations)
                    {
                        captureTimings(filename, elapsed, x);
                        endFile(filename,count,myET1.getText().toString(),"");
                    }
                    else {
                        captureTimings(filename, elapsed, x);
                    }

                }
            }
        });

        //Button to replace word in file.
        myReplace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Set output file name
                final String filename = "performance3AndroidTimingsI" + iterations + "Replacing" + ".txt";

                // Loop to get timing samples
                for (int x = 0; x <= iterations; x++) {
                    // Start timing
                    long startTime = System.nanoTime();

                    //Get instance count from method
                    int count = exists();

                    //Replace instances of word with the new word
                    String newstring = searchex.replace(myET1.getText(), myET2.getText());

                    // Stop timing
                    long stopTime = System.nanoTime();

                    // Calculate elapsed timing
                    long elapsed = stopTime - startTime;

                    if (x == 0) // Initialize and write samples to file, when complete add results.
                    {
                        initializeTimingsFile(filename, "Replace");
                    }
                    else if (x == iterations)
                    {
                        captureTimings(filename, elapsed, x);
                        endFile(filename,count,myET1.getText().toString(), myET2.getText().toString());
                    }
                    else
                    {
                        captureTimings(filename, elapsed, x);
                    }
                }
            }
        });
        //}
    }

    // Count number of words in file
    public int exists()
    {
        String[] words = searchex.split("[ ,.:\t]");
        int count = 0;
        for (int i = 0; i < words.length; i++) {
            if (words[i].equalsIgnoreCase((myET1.getText()).toString())) {
                count++;
            }
        }
        return count;
    }

    // Get file as string from Assets folder
    public String getAssetAsString(AssetManager am)
    {
        StringBuilder buf = null;
        try {
            buf = new StringBuilder();
            InputStream in = am.open("myfile.txt");
            BufferedReader read = new BufferedReader(new InputStreamReader(in));
            String str;

            while ((str = read.readLine()) != null) {
                buf.append(str);
            }

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String x = buf.toString();
        return x;
    }

    // Initialize file by adding header
    public void initializeTimingsFile(String Filename, String doing)
    {
        try{
            File file = new File (Environment.getExternalStorageDirectory(), Filename);
            FileOutputStream out = new FileOutputStream(file, false);
            out.write(("This file contains saved timings for androids performance test 3." + doing + "\n\n").getBytes());
            out.write(("Iterations:\t" + iterations + "\n").getBytes());
            out.write(("\n" + "Iteration\t\t" + "Timing\n").getBytes());
            out.flush();
            out.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // Add line to file with result of iteration
    public void captureTimings(String Filename, long timing, int iteration)
    {
        try{
            File file = new File (Environment.getExternalStorageDirectory(), Filename);
            FileOutputStream out = new FileOutputStream(file, true);
            out.write((iteration + "\t\t\t" + timing + "\n").getBytes());
            out.flush();
            out.close();
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // End file with the instances found and what test was done for how many iterations.
    public void endFile(String Filename, int count, String word, String action){
        try {
            File file = new File(Environment.getExternalStorageDirectory(), Filename);
            FileOutputStream out = new FileOutputStream(file, true);
            if (action != "")
            {
                out.write((count +" instances of "+ word +" where replaced with" + action).getBytes());
            }
            else
            {
                out.write((count +" instances of "+ word +" where found").getBytes());
            }
            out.flush();
            out.close();
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
