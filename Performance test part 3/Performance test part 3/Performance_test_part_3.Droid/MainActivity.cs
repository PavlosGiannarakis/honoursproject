﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content.Res;
using System.IO;
using Android.Media;
using System.Diagnostics;

namespace Performance_test_part_3.Droid
{
	[Activity (Label = "Performance_test_part_3.Droid", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
    {
        int iterations = 100; // Set iteration rate

        protected override void OnCreate (Bundle bundle)
		{
            base.OnCreate (bundle);

			//Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
            string fileInstance = getAssetAsString();

            //Link UI elements to code.
            Button find = FindViewById<Button>(Resource.Id.find);
            Button replace = FindViewById<Button>(Resource.Id.replace);
            EditText one = FindViewById<EditText>(Resource.Id.editText1);
            EditText two = FindViewById<EditText>(Resource.Id.editText2);

            //Set click for the find button
            find.Click += delegate {

                //Next 3 lines set file name and get the path to it in an External Directory.
                string filename = "XamarinAndroidTimingsSearching" + iterations + ".txt";
                var pathDir = global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
                var fullPath = Path.Combine(pathDir.ToString(), "Timings", filename);

                for (int i = 0; i < iterations; i++)// Set for loop to extract timings.
                {
                    Stopwatch stopWatch1 = new Stopwatch(); // Create stopwatch instance

                    stopWatch1.Start(); // Start the stopwatch
                    var start = Java.Lang.JavaSystem.NanoTime(); // Timing in java nanoseconds

                    int count = countInstances(one.Text, fileInstance); // Run the method with the test.
                     
                    var end = Java.Lang.JavaSystem.NanoTime(); // End timing in java nanoseconds
                    stopWatch1.Stop(); // Stop the stopwatch

                    double elapsed = stopWatch1.Elapsed.Milliseconds; // Elapsed Milliseconds from stopwatch.
                    var nanoelapsed = end - start; // Nanoseconds elapsed from Java.

                    /*
                     * Below the if-condition saves the timings to file.
                     */
                    if (i != 0)
                    {
                        captureTiming(fullPath, elapsed, nanoelapsed, i); // Save actual timing per line
                    }
                    else
                    {
                        initializeFile(fullPath, iterations); // Initialize file
                    }
                }
                endFile(fullPath, countInstances(one.Text, fileInstance), one.Text, ""); // Specialized end file
                makeVisible(fullPath); // Make the file visible in the file system, needed in c#
            };

            replace.Click += delegate
            {
                //Next 3 lines set file name and get the path to it in an External Directory.
                string filename = "XamarinAndroidTimingsSearchingAndReplacing" + iterations + ".txt";
                var pathDir = global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
                var fullPath = Path.Combine(pathDir.ToString(), "Timings", filename);

                for (int i = 0; i < iterations; i++)// Set for loop to extract timings.
                {
                    Stopwatch stopWatch1 = new Stopwatch(); // Create stopwatch instance

                    stopWatch1.Start(); // Start the stopwatch
                    var start = Java.Lang.JavaSystem.NanoTime(); // Capture start java system nano time.

                    string newstring = fileInstance.Replace(one.Text, two.Text);
                    int count = countInstances(one.Text, fileInstance);

                    var end = Java.Lang.JavaSystem.NanoTime(); // Capture final java system nano time.
                    stopWatch1.Stop(); // Stop the stopwatch

                    double elapsed = stopWatch1.Elapsed.Milliseconds; // Convert elapsed time to Milliseconds
                    var nanoelapsed = end - start; // Calculate elapsed time in milliseconds.

                    /*
                     * Below the if-condition saves the timings to file.
                     */
                    if (i != 0)
                    {
                        captureTiming(fullPath, elapsed, nanoelapsed, i); // Save actual timing per line
                    }
                    else
                    {
                        initializeFile(fullPath, iterations); // Initialize file
                    }
                }
                endFile(fullPath, countInstances(one.Text, fileInstance), one.Text, two.Text); // Specialized end file
                makeVisible(fullPath); // Make the file visible in the file system, needed in c#
            };
        }

        //Get Asset file and return it as a string (method)
        public string getAssetAsString()
        {
            string content; // String save to.
            AssetManager assets = this.Assets; // Set the asset manager.
            using (StreamReader sr = new StreamReader(assets.Open("myFile.txt")))
            {
                content = sr.ReadToEnd(); // Read all file to string.
            }
            return content; // Return the created string.
        }

        // Test method created for performance test 3. Returns int of counted instances and takes parameters the word to find and file contents.
        public int countInstances(string captured, string file)
        {
            char[] delimiterChars = { ' ', ',', '.', ':', '\t' }; // Char array used to split string.
            string[] words = file.Split(delimiterChars); // Create a string array of words after the contents have been split

            int count = 0; // Set count int to 0

            foreach (var s in words) // Loop through the string array to attempt to find the word given.
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(s, captured, System.Text.RegularExpressions.RegexOptions.IgnoreCase)) // Ignore case
                {
                    count++;// Increase count
                }
            }

            return count; // Return the number of instances
        }

        void captureTiming(string fullPath, double elapsed, long nanoelapsed, int iteration) // Save to file. Takes parameters: timings, iterations and file path.
        {
            StreamWriter file = new StreamWriter(fullPath, true); // Create StreamWriter for file which allows ammendments.
            file.WriteLine(iteration + ":\t\t\t" + elapsed + "\t\t\t" + nanoelapsed); // Write line
            file.Close(); // Close the StreamWriter
        }

        void makeVisible(string fullPath) // Special for visual studio to make files visible
        {
            MediaScannerConnection.ScanFile(this, new string[] { fullPath }, null, null);
        }

        void initializeFile(string fullPath, int iterations) // Initialize file. Sets file header.
        {
            StreamWriter file = new StreamWriter(fullPath, false); // False means, previous files with the same names are overwritten.
            file.WriteLine("This file contains the recorded timings of Performance test part 3 for Xamarin.Android.");
            file.WriteLine("");
            file.WriteLine("Iterations performed: {0}", iterations);
            file.WriteLine("");
            file.WriteLine("The app was run on a device with sdk level: {0}", Build.VERSION.Sdk);
            file.WriteLine("");
            file.WriteLine("Iteration \t Milliseconds \t\t Nanoseconds");
            file.Close();
        }

        void endFile(string fullPath, int count, string word, string action) // End file with the instances found and what test was done for how many iterations.
        {
            StreamWriter file = new StreamWriter(fullPath, true);
            file.WriteLine("");
            if (action != "")
            {
                file.WriteLine("{0} instance of {1} where replaced with {2}", count, word, action);
            }
            else
            {
                file.WriteLine("{0} instance of '{1}' where found", count, word);
            }
            file.Close();
        }

    }
}


