﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Performance_test_part_3.iOS
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton find_btn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton replace_btn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextField1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextField2 { get; set; }

        [Action ("Find_btn_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void Find_btn_TouchUpInside (UIKit.UIButton sender);

        [Action ("Replace_btn_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void Replace_btn_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (find_btn != null) {
                find_btn.Dispose ();
                find_btn = null;
            }

            if (replace_btn != null) {
                replace_btn.Dispose ();
                replace_btn = null;
            }

            if (TextField1 != null) {
                TextField1.Dispose ();
                TextField1 = null;
            }

            if (TextField2 != null) {
                TextField2.Dispose ();
                TextField2 = null;
            }
        }
    }
}