﻿using System;
using System.Diagnostics;
using System.IO;
using UIKit;

namespace Performance_test_part_3.iOS
{
    public partial class ViewController : UIViewController
    {
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        //Set iteration rate
        int iterations = 100;

        //Find Button click 
        partial void Find_btn_TouchUpInside(UIButton sender)
        {
            // Set sample file name
            string filename = string.Format("XamariniOSTimingsFind{0}.txt", iterations);
            var pathDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var fullPath = Path.Combine(pathDir.ToString(), filename);

            //Read from bundled file
            string searchex = File.ReadAllText("myFile.txt");
            
            // Set integer count
            int count = 0;

            //Loop to get samples
            for (int i = 0; i <= iterations; i++)
            {
                // Reset sample to 0 
                count = 0;
                
                Stopwatch stopWatch1 = new Stopwatch(); // Create stopwatch
                stopWatch1.Start(); // Start the stopwatch

                //Get file as string and the word to search for. Then set the chars to split it by.
                string capture = TextField1.Text;
                char[] delimiterChars = { ' ', ',', '.', ':', '\t' };
                string[] words = searchex.Split(delimiterChars);
                
                // loop for each word and increase count if it is equal to the word being searched for. 
                foreach (string s in words)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(s, capture, System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                    {
                        count++;
                    }
                }

                stopWatch1.Stop(); // Stop the stopwatch

                double elapsed = stopWatch1.Elapsed.Milliseconds; // Convert the stopwatch to Milliseconds

                // Set file in the first iteration and add timings after each iteration.
                if (i != 0)
                {
                    captureTiming(fullPath, elapsed, i);
                }
                else
                {
                    initializeFile(fullPath, iterations, "finding");
                }
            }
            //end file with results of test and Confirm completion
            endFile(fullPath, count, TextField1.Text, "");
            find_btn.SetTitle("Process complete", UIControlState.Normal);
        }

        partial void Replace_btn_TouchUpInside(UIButton sender)
        {
            // Set sample file name
            string filename = string.Format("XamariniOSTimingsReplace{0}.txt", iterations);
            var pathDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var fullPath = Path.Combine(pathDir.ToString(), filename);

            // Read bundled file as string and set count to 0
            string replaceex = File.ReadAllText("myFile.txt");
            int count = 0;

            //Get the word to be searched for and specify the chars to split it at
            string capture = TextField1.Text;
            char[] delimiterChars = { ' ', ',', '.', ':', '\t' };

            // Split the String into an array of words
            string[] words = replaceex.Split(delimiterChars);

            //Search words and if an instance of the one reaced for is found increase the count.
            foreach (string s in words)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(s, capture, System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                {
                    count++;
                }
            }

            // Loop to get sample times
            for (int i = 0; i <= iterations; i++)
            {
                Stopwatch stopWatch1 = new Stopwatch(); // Create stopwatch
                stopWatch1.Start(); // Start the stopwatch

                string newstring = replaceex.Replace(TextField1.Text, TextField2.Text);

                stopWatch1.Stop(); // Stop swopwatch

                double elapsed = stopWatch1.Elapsed.Milliseconds; // Convert elapsed time to to Milliseconds

                // Set file and after add a line for each timing added
                if (i != 0)
                {
                    captureTiming(fullPath, elapsed, i);
                }
                else
                {
                    initializeFile(fullPath, iterations, "replacing");
                }
            }

            //Confirm process complete and add a note at the end of the test file
            endFile(fullPath, count, TextField1.Text, TextField2.Text);
            replace_btn.SetTitle("Process complete", UIControlState.Normal);
        }

        // Add timing and iteration to file
        void captureTiming(string fullPath, double elapsed, int iteration)
        {
            StreamWriter file = new StreamWriter(fullPath, true);
            file.WriteLine(iteration + ":\t\t\t" + elapsed);
            file.Close();
        }

        //Initialize file by adding details about test and its parameters
        void initializeFile(string fullPath, int iterations, string part)
        {
            StreamWriter file = new StreamWriter(fullPath, false);
            file.WriteLine("This file contains the recorded timings of Performance test part 3 for Xamarin.iOS when {0} words in a file.", part);
            file.WriteLine("");
            file.WriteLine("Iterations performed: {0}", iterations);
            file.WriteLine("");
            file.WriteLine("Iteration \t Milliseconds ");
            file.Close();
        }

        // End file by adding the results
        void endFile(string fullPath, int count, string word, string action) // End file with the instances found and what test was done for how many iterations.
        {
            StreamWriter file = new StreamWriter(fullPath, true);
            file.WriteLine("");
            if (action != "")
            {
                file.WriteLine("{0} instance of {1} where replaced with {2}", count, word, action);
            }
            else
            {
                file.WriteLine("{0} instance of '{1}' where found", count, word);
            }
            file.Close();
        }
    }
}

